﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * As a PMO, I want to have a sprint summary report that reports the status, time spent, 
 * and any restimates for each user story so that I can understand the progress that is 
 * being made each week. 
 * 
 * Coding notes: I can modify the code I used for the story backlog to read all of this in 
 * -- or for the gui part, it will just hold everything in that and we can read it from there.
 * 
 * Further notes to self: a lot of this could be converted into extensions, I think
 */
namespace SprintReportManager
{
    class Program
    {
        public static List<SprintReport> CompletedSprints = new List<SprintReport>();
        public class UserStory
        {
            public string _title { get; set; }
            public string _status { get; set; }
            public string _timeSpent { get; set; }
            public string _timeEstimate { get; set; }
            public int _storyPoint { get; set; }
            public UserStory(string title, string status, string timespent, string timeestimate, int pointValue)
            {
                _title = title;
                _status = status;
                _timeSpent = timespent;
                _timeEstimate = timeestimate;
                _storyPoint = pointValue;
            }
        }
        public class SprintReport
        {
            string line = new string('-', 10);
            string _title = "";
            List<UserStory> userStories = new List<UserStory>();
            public UserStory _story { get; set; }
            public SprintReport(List<UserStory> usl, string title) { this.userStories = usl; this._title = title; }
            int _sNumber = 1;
            public void PrintReport()
            {
                Console.WriteLine(_title);
                _sNumber = 1;
                //starting with just a console
                foreach (UserStory story in userStories)
                {
                    Console.WriteLine("{0}) {1}:\n{2}\nTime Estimate: {3}\nTime Spent: {4}\nStory Point: {5}", _sNumber, story._title, story._status, story._timeEstimate, story._timeSpent, story._storyPoint);
                    ++_sNumber;
                    Console.WriteLine();
                }
                Console.WriteLine($"Completed Story Points: {GetCurrentPointCompletion()} / {GetTotalPoints()}");
            }

            public void PrintSpecificStory(UserStory story)
            {
                Console.WriteLine("\n{0}\n{1}:\n{2}\nTime Estimate: {3}\nTime Spent: {4}\nStory Point: {5}", line, story._title, story._status, story._timeEstimate, story._timeSpent, story._storyPoint);
                SelectStory();
            }

            public void SelectStory()
            {
                string uIn = "";
                string _save = "";
                int _storyNumber = 1;

                Console.WriteLine("\n{0}\nWhich story do you wish to modify? [1-{1}, or x to exit]", line, _sNumber - 1);
                uIn = Console.ReadLine();
                if (uIn.ToLower() == "0")
                {
                    TitleMod();
                }
                else if (uIn.ToLower() == "x")
                {
                    Console.WriteLine("Save Changes? [y/n]");
                    _save = Console.ReadLine();
                    if (_save == "y" || _save == "yes")
                    {
                        //SaveChanges();
                        Console.WriteLine("\n{0}\nChanges saved.", line);
                    }
                    else if (_save == "n" || _save == "no")
                    {
                        Console.WriteLine("\n{0}\nChanges not saved.", line);
                    }
                    else
                    {
                        Console.WriteLine("\n{0}\nBad Key", line);
                        SelectStory();
                    }
                }
                else if (uIn.ToLower() != "x")
                {
                    try
                    {
                        _storyNumber = Int16.Parse(uIn);
                        ModifyStory(userStories[_storyNumber - 1]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Bad entry: {0}", e.Message);
                        SelectStory();

                    }
                }
            }


            public void ModifyStory(UserStory story)
            {
                string uInput;
                Console.WriteLine("What part of the story needs to be modified? [1=Status, 2=Time Spent, 3=Time Estimate, 4=Back]");
                uInput = Console.ReadLine();

                if (uInput == "1")
                {
                    Console.WriteLine("Modifying status...");
                    ChangeStatus(story);
                }
                else if (uInput == "2")
                {
                    Console.WriteLine("Modifying time spent...");
                    ChangeTimeSpent(story);
                }
                else if (uInput == "3")
                {
                    Console.WriteLine("Modifying time estimate...");
                    ReEstimateTime(story);
                }
                else if (uInput == "4")
                {
                    Console.Write("\n\n");
                    PrintReport();
                }
                else { Console.WriteLine("Bad entry."); }
            }
            public void TitleMod()
            {
                string title = "";
                Console.WriteLine("Change title to: ");
                title = Console.ReadLine();
                _title = title;
                PrintReport();
            }
            public void ChangeStatus(UserStory story)
            {
                string _oldStatus = story._status;
                string _newStatus = "";
                Console.WriteLine("The old Status was: {0}", _oldStatus);
                Console.Write("Enter a new Status: ");
                _newStatus = Console.ReadLine();
                story._status = _newStatus;
                Console.WriteLine(line);
                //PrintSpecificStory(story);
                PrintReport();
            }
            public void ChangeTimeSpent(UserStory story)
            {
                string _oldTime = story._timeSpent;
                string _newTime = "";
                Console.WriteLine("The old Time Spent was: {0}", _oldTime);
                Console.Write("Enter a new Time Spent: ");
                _newTime = Console.ReadLine();
                story._timeSpent = _newTime;
                Console.WriteLine(line);
                //PrintSpecificStory(story);
                PrintReport();
            }
            public void ReEstimateTime(UserStory story)
            {
                string _oldTime = story._timeEstimate;
                string _newTime = "";
                Console.WriteLine("The old estimate was: {0}", _oldTime);
                Console.Write("Enter a new time Estimate: ");
                _newTime = Console.ReadLine();
                story._timeEstimate = _newTime;
                Console.WriteLine(line);
                //PrintSpecificStory(story);
                PrintReport();
            }
            public void SaveChanges()
            {
                // call writexml 
                Console.WriteLine("\n{0}\n(NO SAVE FUNCTION YET)\nSaved.\n{0}", line);
            }

            public int GetCurrentPointCompletion()
            {
                if (userStories.Count == 0)
                    return 0;
                else
                {
                    int completedPoints = 0;
                    List<int> storyPoints = new List<int>();
                    foreach (var story in userStories)
                    {
                        if (story._status == "completed")
                        {
                            storyPoints.Add(story._storyPoint);
                        }
                    }
                    foreach (var point in storyPoints)
                    {
                        completedPoints += point;
                    }
                    return completedPoints;
                }
            }

            public int GetTotalPoints()
            {
                int total = 0;
                foreach(var story in userStories)
                {
                    total += story._storyPoint;
                }
                return total;
            }

        }
        static void Main(string[] args)
        {
            UserStory s1 = new UserStory("S1", "in progress", "9", "12", 8);
            UserStory s2 = new UserStory("S2", "completed", "8", "8", 5);
            UserStory s3 = new UserStory("S3", "in progress", "9", "7", 3);
            UserStory s4 = new UserStory("S4", "completed", "2", "12", 2);
            List<UserStory> userStories = new List<UserStory> { s1, s2, s3, s4 };


            SprintReport sprint1 = new SprintReport(userStories, "Sprint 1");
            SprintReport sprint2 = new SprintReport(userStories, "Sprint 2");
            SprintReport sprint3 = new SprintReport(userStories, "Sprint 3");

            
            CompletedSprints.Add(sprint1);
            CompletedSprints.Add(sprint2);
                Console.WriteLine("--SPRINT REPORT MANAGER--");
            Console.WriteLine();
            sprint3.PrintReport();
            Console.WriteLine();
            if(CompletedSprints.Count == 0)
            {
                Console.WriteLine("Velocity: Can not calculate, no sprints have been completed.");
            }
            else
            {
                Console.WriteLine($"Velocity: {GetVelocity()}");
            }
            sprint3.SelectStory();
        }

        static int GetVelocity()
        {
            int velocity = 0;
            if(CompletedSprints.Count != 0)
            {
                foreach(var sprint in CompletedSprints)
                {
                    velocity += sprint.GetCurrentPointCompletion();
                }
                return (int)(velocity / CompletedSprints.Count());
            }

            return 0;
        }
    }
}
