﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace BacklogGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<TaskItem> TaskList;
        private ObservableCollection<string> itemList;

        public MainWindow()
        {
            InitializeComponent();
            TaskList = new ObservableCollection<TaskItem>();
            itemList = new ObservableCollection<string>();
            this.DataContext = itemList;
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            tbSprintName.Text = "";
            tbStoryDesc.Text = "";
            tbFinalNotes.Text = "";
            lbTaskItems.Items.Clear();
            tbItemToAdd.Text = "";
            tbCompletion.Text = "";
        }

        private void BtnAddItem_Click(object sender, RoutedEventArgs e)
        {
            if(tbItemToAdd.Text !=null)
                lbTaskItems.Items.Add(tbItemToAdd.Text);
            itemList.Add(tbItemToAdd.Text);
            tbItemToAdd.Text = "";
        }

        private void BtnRemoveItem_Click(object sender, RoutedEventArgs e)
        {
            if (lbTaskItems.SelectedIndex != -1)
            {
                itemList.RemoveAt(lbTaskItems.SelectedIndex);
                lbTaskItems.Items.RemoveAt(lbTaskItems.SelectedIndex);
            }
        }

        private void BtnAddStory_Click(object sender, RoutedEventArgs e)
        {
            List<string> stringList = new List<string>();
            ObservableCollection<string> nCollection = new ObservableCollection<string>();

            string temph = "";
            foreach (var item in lbTaskItems.Items)
            {
                temph = item.ToString();
                stringList.Add(temph);
            }

            double time = 0;
            if (tbCompletion.Text != "")
                time = Convert.ToDouble(tbCompletion.Text);

            TaskItem task = new TaskItem(tbSprintName.Text, tbStoryDesc.Text, stringList, time, tbFinalNotes.Text);

            TaskList.Add(task);
            lbAllTasks.Items.Add(tbSprintName.Text);

            tbSprintName.Text = "";
            tbStoryDesc.Text = "";
            tbFinalNotes.Text = "";
            lbTaskItems.Items.Clear();
            tbItemToAdd.Text = "";
            tbCompletion.Text = "";
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            ProjectItem p = new ProjectItem();
           foreach(var item in TaskList)
                p.taskItems.Add(item);
            WriteXML(p);
        }

        private void BtnEditItem_Click(object sender, RoutedEventArgs e)
        {

        }

        public static void WriteXML(ProjectItem project)
        {
            string fileName = "testing1";

            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(ProjectItem));
            System.IO.FileStream file = System.IO.File.Create(fileName + ".xml");
            try
            {
                writer.Serialize(file, project);
                MessageBox.Show("File saved");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SelectItem_Click(object sender, RoutedEventArgs e)
        {
            if(lbTaskItems.SelectedIndex != -1)
            {
                string selectedItem = (string)lbTaskItems.SelectedItem;
                tbItemToAdd.Text = selectedItem;
            }
        }

        private void BtnLoadTask_Click(object sender, RoutedEventArgs e)
        {
            TaskItem task = new TaskItem();
            string sName = (string)lbAllTasks.SelectedItem;
            foreach (var item in TaskList)
                if (item.taskName == sName)
                    task = item;
            tbSprintName.Text = task.taskName;
            tbStoryDesc.Text = task.taskDescription;
            tbFinalNotes.Text = task.taskNotes;
            foreach (var item in task.taskList)
                lbTaskItems.Items.Add(item);
            tbItemToAdd.Text = "";
            tbCompletion.Text = "" + task.estimatedTime;
        }
    }
}
