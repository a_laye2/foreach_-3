﻿/*
 Coder: Alex Laye
 Program name: backlog_tracker
 Date: March 3, 2019
 Purpose: All class files needed for main program. Built for holding data for read in and writing

 
 Modified: March 11, 2019
 - Added completion time for entire project
 
  */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace BacklogGUI
{
    [Serializable]
    public class ProjectItem
    {
        public List<TaskItem> taskItems = new List<TaskItem>();
        public string projectName { get; set; }
        public double completionTimeEstimate { get; set; }
        public ProjectItem() { }

    }
    [Serializable]
    public class TaskItem
    {
        public string taskName { get; set; }
        public string taskDescription { get; set; }
        public string taskItem { get; set; }
        public List<string> taskList = new List<string>();
        public double estimatedTime { get; set; }
        public string taskNotes { get; set; }
        public TaskItem() { }
        public TaskItem(string name, string description, List<string> list, double estTime, string notes)
        {
            this.taskName = name;
            this.taskDescription = description;
            this.estimatedTime = estTime;
            this.taskList = list;
            this.taskNotes = notes;
        }
        public void Save(string fileName)
        {
            IFormatter formatter = new BinaryFormatter();
            using (FileStream stream = new FileStream(fileName, FileMode.Create))
            {
                formatter.Serialize(stream, this);
            }
        }

    }
}
