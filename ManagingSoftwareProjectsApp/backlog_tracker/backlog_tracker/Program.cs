﻿/*
 Coder: Alex Laye
 Program name: backlog_tracker
 Date: March 3, 2019
 Purpose: to capture/maintain a product backlog that includes relative estimates (by team member) so that I can establish a benchmark for comparison purposes

 
 Modified: March 11, 2019
 Fixed read in for the XML files. Ready for UI integrration
 
  */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace backlog_tracker
{
    class Program
    {
        private void OnSave(ProjectItem project)
        {
            WriteXML(project);
            Console.WriteLine("{0} written to file.", project.projectName);
        }

        public static void loadOldProject()
        {
            string fileName = "";
            Console.Write("Enter the name of the file: ");
            fileName = Console.ReadLine() + ".xml";
            try
            {
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    IFormatter formatter = new BinaryFormatter();
                    System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(ProjectItem));
                    System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                    
                    ProjectItem projectItem = (ProjectItem)reader.Deserialize(file);

                    Console.WriteLine(file.ReadToEnd());
                    Console.WriteLine(projectItem.taskItems[0].taskName);
                    Console.WriteLine("Task File Loaded:\n\n");

                    foreach (var i in projectItem.taskItems)
                    {
                        Console.WriteLine(i.taskName);
                        Console.WriteLine(i.taskDescription);
                        foreach (var j in i.taskList)
                        {
                            Console.WriteLine(j.ToString());
                        }
                        Console.WriteLine("Estimated hours: {0}", i.estimatedTime);
                        Console.WriteLine(i.taskNotes);
                    }
                    Console.WriteLine("Estimated completion hours is {0}.",projectItem.completionTimeEstimate);
                    file.Close();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        // write data to file -- xml in this case
        public static void WriteXML(ProjectItem project)
        {
            string fileName = "";

            Console.Write("Name the file: ");
            fileName = Console.ReadLine();

            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(ProjectItem));
            System.IO.FileStream file = System.IO.File.Create(fileName + ".xml");
            try
            {
                writer.Serialize(file, project);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                file.Close();
            }
        }
       
        // calling on to start a new project
        public static void startNewProject()
        {
            ProjectItem project = new ProjectItem();
            TaskItem task = new TaskItem();

            string exitChar = "";
            string exitChar2 = "";
            string tempHolder = "";
            double timeHolder = 0;
            while (exitChar != "!")
            {
                Console.Write("Enter story name for a new story, or '!' to exit: ");
                exitChar = Console.ReadLine();
                if (exitChar == "!")
                    break;
                else
                    task.taskName = exitChar;
                Console.Write("Enter story description: ");
                task.taskDescription = Console.ReadLine();

                while (exitChar2 != "!")
                {
                    Console.Write("Enter story list item (or '!' to end list): ");
                    tempHolder = Console.ReadLine();
                    if (tempHolder != "!")
                    {
                        task.taskList.Add(tempHolder);
                    }
                    else if (tempHolder == "!")
                        break;
                }
                Console.Write("Enter estimated time for completion in hours (numbers only): ");
                task.estimatedTime = Int16.Parse(Console.ReadLine());
                timeHolder += task.estimatedTime;
                Console.Write("Enter final notes on story item: ");
                task.taskNotes = Console.ReadLine();

                project.completionTimeEstimate = timeHolder;
                project.taskItems.Add(task);
            }
            WriteXML(project);
        }
        // initializer
        public static void myInit()
        {
            string projectChoice = "";
        
            Console.WriteLine("[N]ew or [O]ld project or E[x]it?");
            projectChoice = Console.ReadLine();

            if (projectChoice.ToLower() == "n")
                startNewProject();
            else if (projectChoice.ToLower() == "o")
                loadOldProject();
            else if (projectChoice.ToLower() == "x")
                System.Environment.Exit(0);
            else
            {
                Console.WriteLine("Bad entry");
                myInit();
            }
        }

        // main program; only calling the init
        static void Main(string[] args)
        {
            myInit();
        }
    }
}
