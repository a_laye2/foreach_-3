﻿/*
 Coder: Alex Laye
 Program name: backlog_tracker
 Date: March 3, 2019
 Purpose: All class files needed for main program. Built for holding data for read in and writing

 
 Modified: March 11, 2019
 - Added completion time for entire project
 
  */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace backlog_tracker
{
    [Serializable]
    public class ProjectItem
    {
        public List<TaskItem> taskItems = new List<TaskItem>();
        public string projectName { get; set; }
        public double completionTimeEstimate { get; set; }
        public ProjectItem()
        {
            projectName = "No projects yet!";
            completionTimeEstimate = 0.0;
        }

    }

    public class TaskItem
    {
        public string taskName { get; set; }
        public string taskDescription { get; set; }
        public string taskItem { get; set; }
        public List<string> taskList = new List<string>();
        public double estimatedTime { get; set; }
        public double timeElapsed { get; set; }
        public double extraTime { get; set; }
        public string taskNotes { get; set; }

        public void Save(string fileName)
        {
            IFormatter formatter = new BinaryFormatter();
            using (FileStream stream = new FileStream(fileName, FileMode.Create))
            {
                formatter.Serialize(stream, this);
            }
        }

        public TaskItem(string name)
        {
            taskName = name;
        }

        public TaskItem()
        {
            
        }

        public override string ToString()
        {
            return $"Story: {taskName}\n\nDescription: {taskDescription}\n\nEstimated Time: {estimatedTime}\n\nElapsed Time: {timeElapsed}\n\nExtra Time: {extraTime}";
        }
    }
}
