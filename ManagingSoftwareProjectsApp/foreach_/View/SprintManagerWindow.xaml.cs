﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using foreach_.Model;

namespace foreach_.View
{
    /// <summary>
    /// Interaction logic for SprintManagerWindow.xaml
    /// </summary>
    public partial class SprintManagerWindow : Window
    {
        public static void WriteXML(SprintReport report, string filename) {
            string fileName = filename;

            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(SprintReport));
            System.IO.FileStream file = System.IO.File.Create(fileName + ".xml");
            try {
                writer.Serialize(file, report);
                MessageBox.Show("File saved");
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        public void GetVelocity()
        {
            SprintReport report = new SprintReport();
            foreach (var item in storyCollection)
                report.userStories.Add(item);

            int velocity = 0;
            if (report._completedSprints.Count != 0)
            {
                foreach (var sprint in report._completedSprints)
                {
                    velocity += sprint._storyPoint;
                }
               velocity = (int)(velocity / report._completedSprints.Count());
            }

            else
                velocity = 0;



            lblCurrentVelocity.Content = velocity;
        }

        public void ClearText() {
            tbTitle.Text = "";
            tbTimeSpent.Text = "";
            tbTimeEstimate.Text = "";
            tbPointValue.Text = "";
            rbComplete.IsChecked = false;
            rbIncomplete.IsChecked = true;
        }

        public static ObservableCollection<UserStory> storyCollection;

        public SprintManagerWindow() {
            InitializeComponent();
            storyCollection = new ObservableCollection<UserStory>();
        }

        private void BtnAddSprint_Click(object sender, RoutedEventArgs e) {
            storyCollection.Add(new UserStory(tbTitle.Text, "status", tbTimeSpent.Text, tbTimeEstimate.Text, 0)); // note: need to fix the status and the time...
            lbSprintList.Items.Add(tbTitle.Text);
            ClearText();
        }

        private void BtnClearSprint_Click(object sender, RoutedEventArgs e) {
            ClearText();
        }

        private void BtnEditsprint_Click(object sender, RoutedEventArgs e) {
            
        }

        private void BtnSaveSprint_Click(object sender, RoutedEventArgs e) {
            SprintReport report = new SprintReport();
            foreach(var item in storyCollection)
                report.userStories.Add(item);
            WriteXML(report, tbFile.Text);
            MessageBox.Show("Sprint saved!");
        }

        private void BtnLoadSprint_Click(object sender, RoutedEventArgs e) {
            // first, clear all items:
            storyCollection = new ObservableCollection<UserStory>();
            lbSprintList.Items.Clear();

            // name of the file to load; hardcoded for the moment, can be changed to userinput
            string oldFile = tbFile.Text;
            string fileName = oldFile + ".xml";
            // load the file
            try {
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                    IFormatter formatter = new BinaryFormatter();
                    System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(SprintReport));
                    System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                    SprintReport report = (SprintReport)reader.Deserialize(file);
                    foreach (var i in report.userStories)
                        storyCollection.Add(i);
                    file.Close();
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            // fill the listbox with story items from the collection
            foreach (var item in storyCollection)
                lbSprintList.Items.Add(item._title);

            GetVelocity();
        }

        private void BtnVelocity_Click(object sender, RoutedEventArgs e)
        {
            GetVelocity();
        }
    }
}
