﻿using foreach_.Model;
using foreach_.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace foreach_.View
{
    /// <summary>
    /// Interaction logic for UserWindow.xaml
    /// </summary>
    public partial class UserWindow : Window
    {
        ViewModelUser vm;
        public UserWindow(User selectedUser)
        {
            InitializeComponent();
            vm = new ViewModelUser(selectedUser);
            this.DataContext = vm;
        }
    }
}
