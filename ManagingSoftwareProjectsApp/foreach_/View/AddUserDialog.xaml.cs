﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace foreach_.View
{
    /// <summary>
    /// Interaction logic for AddUserDialog.xaml
    /// </summary>
    public partial class AddUserDialog : Window
    {
        public new string Name { get; set; }
        public AddUserDialog()
        {
            InitializeComponent();
        }

        private void OnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (txtName.Text != "")
            {
                Name = txtName.Text;
                this.Close();
            }
            else
            {
                MessageBox.Show("You must enter a name", "Add User Error");
            }

        }
    }
}
