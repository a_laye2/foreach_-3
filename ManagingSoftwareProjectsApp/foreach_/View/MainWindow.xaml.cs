﻿using backlog_tracker;
using foreach_.Model;
using foreach_.View;
using foreach_.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace foreach_
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ManagementObject mainObj;
        ViewModelMain vm;
        public MainWindow()
        {
            InitializeComponent();
            mainObj = new ManagementObject();
            vm = new ViewModelMain();
            this.DataContext = vm;
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            //initialize an opendialog object
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";
            openDialog.Filter = "XML Files (*.xml)|*.xml";
            openDialog.InitialDirectory = Directory.GetCurrentDirectory();
            //file has been chosen
            if (openDialog.ShowDialog().Equals(true))
            {
                //get the path selected and open file
                string path = System.IO.Path.GetFullPath(openDialog.FileName);
                // Now we can read the serialized object ...  
                System.Xml.Serialization.XmlSerializer reader =
                new System.Xml.Serialization.XmlSerializer(typeof(backlog_tracker.ProjectItem));
                System.IO.StreamReader file = new System.IO.StreamReader(
                    path);
                backlog_tracker.ProjectItem projectItem = (backlog_tracker.ProjectItem)reader.Deserialize(file);
                lbStories.ItemsSource = projectItem.taskItems;
                txtName.Text = projectItem.projectName;
                txtTime.Text = projectItem.completionTimeEstimate.ToString();
            }
        }

        private void BtnBacklog_Click(object sender, RoutedEventArgs e)
        {
            BacklogWindow blgWin = new BacklogWindow();
            blgWin.ShowDialog();



        }

        private void BtnManager_Click(object sender, RoutedEventArgs e)
        {
            SprintManagerWindow smw = new SprintManagerWindow();
            smw.ShowDialog();
        }

    }
}
