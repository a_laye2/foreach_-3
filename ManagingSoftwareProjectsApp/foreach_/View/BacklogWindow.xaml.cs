﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using foreach_.Model;

namespace foreach_.View
{
    /// <summary>
    /// Interaction logic for BacklogWindow.xaml
    /// </summary>
    public partial class BacklogWindow : Window
    {
        private ObservableCollection<TaskItem> TaskList;
        private ObservableCollection<string> itemList;

        public BacklogWindow()
        {
            InitializeComponent();
            TaskList = new ObservableCollection<TaskItem>();
            itemList = new ObservableCollection<string>();
            this.DataContext = itemList;
        }


        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearText();
        }

        private void BtnAddItem_Click(object sender, RoutedEventArgs e)
        {
            if (tbItemToAdd.Text != null)
                lbTaskItems.Items.Add(tbItemToAdd.Text);
            itemList.Add(tbItemToAdd.Text);
            tbItemToAdd.Text = "";
        }

        private void BtnRemoveItem_Click(object sender, RoutedEventArgs e)
        {
            if (lbTaskItems.SelectedIndex != -1)
            {
                itemList.RemoveAt(lbTaskItems.SelectedIndex);
                lbTaskItems.Items.RemoveAt(lbTaskItems.SelectedIndex);
            }
        }

        private void BtnAddStory_Click(object sender, RoutedEventArgs e)
        {
            List<string> stringList = new List<string>();
            ObservableCollection<string> nCollection = new ObservableCollection<string>();

            string temph = "";
            foreach (var item in lbTaskItems.Items)
            {
                temph = item.ToString();
                stringList.Add(temph);
            }

            double time = 0;
            if (tbCompletion.Text != "")
                time = Convert.ToDouble(tbCompletion.Text);

            TaskItem task = new TaskItem(tbSprintName.Text, tbStoryDesc.Text, stringList, time, tbFinalNotes.Text);

            TaskList.Add(task);
            lbAllTasks.Items.Add(tbSprintName.Text);

            ClearText();
        }

        public void ClearText()
        {
            tbSprintName.Text = "";
            tbStoryDesc.Text = "";
            tbFinalNotes.Text = "";
            lbTaskItems.Items.Clear();
            tbItemToAdd.Text = "";
            tbCompletion.Text = "";
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            ProjectItem p = new ProjectItem();
            foreach (var item in TaskList)
                p.taskItems.Add(item);
            WriteXML(p, txtLoadSave.Text);
        }

        private void BtnEditItem_Click(object sender, RoutedEventArgs e)
        {

        }

        public static void WriteXML(ProjectItem project, string filestring)
        {
            string fileName = filestring;

            System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(typeof(ProjectItem));
            System.IO.FileStream file = System.IO.File.Create(fileName + ".xml");
            try {
                writer.Serialize(file, project);
                MessageBox.Show("File saved");
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                file.Close();
            }
        }

        private void SelectItem_Click(object sender, RoutedEventArgs e) {
            if (lbTaskItems.SelectedIndex != -1) {
                string selectedItem = (string)lbTaskItems.SelectedItem;
                tbItemToAdd.Text = selectedItem;
            }
        }

        private void BtnLoadTask_Click(object sender, RoutedEventArgs e) {
            TaskItem task = (TaskItem)lbAllTasks.SelectedItem;
            foreach (var item in TaskList)
                if (item.taskName == task.taskName)
                    task = item;
            tbSprintName.Text = task.taskName;
            tbStoryDesc.Text = task.taskDescription;
            tbFinalNotes.Text = task.taskNotes;
            foreach (var item in task.taskList)
                lbTaskItems.Items.Add(item);
            tbItemToAdd.Text = "";
            tbCompletion.Text = "" + task.estimatedTime;
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            // firtst, clear everything since we're loading:
            ClearText();
            lbAllTasks.Items.Clear();
            lbAllTasks.Items.Clear();
            TaskList = new ObservableCollection<TaskItem>();
            itemList = new ObservableCollection<string>();

            ProjectItem projectItem = new ProjectItem();

            // next create the filestring to load the file:
            string oldFile = txtLoadSave.Text;
            string fileName = oldFile + ".xml";

            // load the file:
            try
            {
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {

                    System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(ProjectItem));
                    System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                    ProjectItem report = (ProjectItem)reader.Deserialize(file);
                    foreach (var i in report.taskItems)
                    {
                        //MessageBox.Show(i.taskName);
                        TaskList.Add(i);
                    }

                    file.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            foreach (var item in TaskList)
                lbAllTasks.Items.Add(item.taskName);

        }
    }
}
