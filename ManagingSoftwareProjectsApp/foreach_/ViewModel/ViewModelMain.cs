﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using backlog_tracker;
using foreach_.Helper;
using foreach_.Model;
using foreach_.View;
using Microsoft.Win32;

namespace foreach_.ViewModel
{
    class ViewModelMain : DependencyObject
    { 



        public backlog_tracker.ProjectItem CurrentProject
        {
            get { return (backlog_tracker.ProjectItem)GetValue(CurrentProjectProperty); }
            set { SetValue(CurrentProjectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentProject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentProjectProperty =
            DependencyProperty.Register("CurrentProject", typeof(backlog_tracker.ProjectItem), typeof(ViewModelMain), new PropertyMetadata(new backlog_tracker.ProjectItem()));


        public ObservableCollection<User> Users
        {
            get { return (ObservableCollection<User>)GetValue(UsersProperty); }
            set { SetValue(UsersProperty, value); }
        }

        // Using a DependencyProperty as the backing store for statistics.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UsersProperty =
            DependencyProperty.Register("Users", typeof(ObservableCollection<User>), typeof(ViewModelMain), new PropertyMetadata(null));



        public User SelectedUser
        {
            get { return (User)GetValue(SelectedUserProperty); }
            set { SetValue(SelectedUserProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedStat.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedUserProperty =
            DependencyProperty.Register("SelectedUser", typeof(User), typeof(ViewModelMain), new PropertyMetadata(new User("")));

        public RelayCommand displayUserCommand { get; set; }
        public RelayCommand removeUserCommand { get; set; }
        public RelayCommand addUserCommand { get; set; }
        public RelayCommand OpenProjectCommand { get; set; }

        public ViewModelMain()
        {
            Users = new ObservableCollection<User>();
            User user = new User("Michael");
            user.Stories.Add(new backlog_tracker.TaskItem("This is a fake story"));
            user.Stories.Add(new backlog_tracker.TaskItem("This is a fake story"));
            user.Stories.Add(new backlog_tracker.TaskItem("This is a fake story"));
            Users.Add(user);

            User user2 = new User("Steve");
            user2.Stories.Add(new backlog_tracker.TaskItem("This is a fake story"));
            Users.Add(user2);

            displayUserCommand = new RelayCommand(DisplayUser);
            removeUserCommand = new RelayCommand(RemoveUser);
            addUserCommand = new RelayCommand(AddUser);
            OpenProjectCommand = new RelayCommand(OpenProject);

            CurrentProject = new backlog_tracker.ProjectItem();
        }

        private void DisplayUser(object obj)
        {
            if(SelectedUser != null && SelectedUser.Name != "")
            {
                var win = new UserWindow(SelectedUser);
                win.ShowDialog();
            }
            else
            {
                MessageBox.Show("You must select a user", "Display User Error");
            }
        }

        private void RemoveUser(object obj)
        {
            if (SelectedUser != null && SelectedUser.Name != "")
            {
                Users.Remove(SelectedUser);
            }
            else
            {
                MessageBox.Show("You must select a user", "Remove User Error");
            }
            
        }

        private void AddUser(object obj)
        {
            var win = new AddUserDialog();
            win.ShowDialog();
            if(win.Name != null)
            {
                var checkForUser = Users.Any(u => u.Name == win.Name);
                if(!checkForUser)
                {
                    Users.Add(new User(win.Name));
                    MessageBox.Show($"User added successfully", "Add User Successful");
                }
                else
                {
                    MessageBox.Show("User already exists", "Add User Failed");
                }
            }
        }

        private void OpenProject(object obj)
        {
            //initialize an opendialog object
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";
            openDialog.Filter = "XML Files (*.xml)|*.xml";
            openDialog.InitialDirectory = Directory.GetCurrentDirectory();
            //file has been chosen
            if (openDialog.ShowDialog().Equals(true))
            {
                //get the path selected and open file
                string path = Path.GetFullPath(openDialog.FileName);
                // Now we can read the serialized object ...  
                System.Xml.Serialization.XmlSerializer reader =
                new System.Xml.Serialization.XmlSerializer(typeof(backlog_tracker.ProjectItem));
                System.IO.StreamReader file = new System.IO.StreamReader(
                    path);
                backlog_tracker.ProjectItem projectItem = (backlog_tracker.ProjectItem)reader.Deserialize(file);
                CurrentProject.projectName = projectItem.projectName;
                CurrentProject.taskItems.Clear();
                foreach (var item in projectItem.taskItems)
                {
                    CurrentProject.taskItems.Add(item);
                }
            }    
        }
    }
}
