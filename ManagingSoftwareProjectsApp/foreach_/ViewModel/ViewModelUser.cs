﻿using foreach_.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using backlog_tracker;

namespace foreach_.ViewModel
{
    class ViewModelUser : DependencyObject
    {



        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Name.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NameProperty =
            DependencyProperty.Register("Name", typeof(string), typeof(ViewModelUser), new PropertyMetadata(""));


        public ObservableCollection<backlog_tracker.TaskItem> Stories
        {
            get { return (ObservableCollection<backlog_tracker.TaskItem>)GetValue(StoriesProperty); }
            set { SetValue(StoriesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StoriesProperty =
            DependencyProperty.Register("Stories", typeof(ObservableCollection<backlog_tracker.TaskItem>), typeof(ViewModelUser), new PropertyMetadata(null));



        public backlog_tracker.TaskItem SelectedStory
        {
            get { return (backlog_tracker.TaskItem)GetValue(SelectedStoryProperty); }
            set { SetValue(SelectedStoryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedStoryProperty =
            DependencyProperty.Register("SelectedStory", typeof(backlog_tracker.TaskItem), typeof(ViewModelUser), new PropertyMetadata(new backlog_tracker.TaskItem("")));



        public ViewModelUser(User user)
        {
            Stories = new ObservableCollection<backlog_tracker.TaskItem>();
            Stories = user.Stories;
            Name = user.Name;
            
        }

        private static T findAncestor<T>(DependencyObject current)
            where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }
    }
}
