﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace foreach_.Model
{
    public class Story
    {
        string _Description { get; set; }
        int _EstimatedTime { get; set; }
        int _TimeElapsed { get; set; }
        int _ExtraTime { get; set; }

        public Story()
        {

        }
        public Story(string desc, int est, int elap, int extra)
        {
            _Description = desc;
            _EstimatedTime = est;
            _TimeElapsed = elap;
            _ExtraTime = extra;
        }

        public override string ToString()
        {
            return $"Story: {_Description}\nEstimated Time: {_EstimatedTime}\nTime Elapsed: {_TimeElapsed}\nExtra Time: {_ExtraTime}";
        }
    }
}
