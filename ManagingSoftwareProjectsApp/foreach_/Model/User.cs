﻿using backlog_tracker;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace foreach_.Model
{
    [Serializable]
    public class User
    {
        public string Name { get; set; }

        public ObservableCollection<backlog_tracker.TaskItem> Stories { get; set; } 

        public User(string n)
        {
            Name = n;
            Stories = new ObservableCollection<backlog_tracker.TaskItem>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
