﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace foreach_.Model
{
    public class UserStory
    {
        public string _isCompleted { get; set; }
        public string _title { get; set; }
        public string _status { get; set; }
        public string _timeSpent { get; set; }
        public string _timeEstimate { get; set; }
        public int _storyPoint { get; set; }
        public UserStory() { }
        public UserStory(string title, string status, string timespent, string timeestimate, int pointValue)
        {
            _title = title;
            _status = status;
            _timeSpent = timespent;
            _timeEstimate = timeestimate;
            _storyPoint = pointValue;
        }
    }
    public class SprintReport
    {
        int _sprintNumber { get; set; }

        int _totalStoryPoints { get; set; }
        int _completedStoryPoints { get; set; }
        double _velocity { get; set; }

        public List<UserStory> _completedSprints = new List<UserStory>();
        string _title { get; set; }
        public List<UserStory> userStories = new List<UserStory>();
        public SprintReport() { }
        public SprintReport(List<UserStory> usl)
        {
            this.userStories = usl;
            //this._title = title;

            //CheckComplete();
            //_velocity = GetVelocity();
            //GetCurrentPointCompletion();
        }
        public void CheckComplete()
        {
            foreach (var item in userStories)
            {
                if (item._isCompleted.ToLower() == "yes" || item._isCompleted.ToLower() == "completed" || item._isCompleted.ToLower() == "complete")
                {
                    _completedSprints.Add(item);
                    userStories.Remove(item);
                }
            }
        }

        public void GetCurrentPointCompletion()
        {
            if (userStories.Count == 0)
                _totalStoryPoints = 0;
            else
            {
                int completedPoints = 0;
                List<int> storyPoints = new List<int>();
                foreach (var story in userStories)
                {
                    if (story._status == "completed")
                    {
                        storyPoints.Add(story._storyPoint);
                    }
                }
                foreach (var point in storyPoints)
                {
                    completedPoints += point;
                }
                _totalStoryPoints = completedPoints;
            }
        }

        public double GetVelocity()
        {
            int velocity = 0;
            if (_completedSprints.Count != 0)
            {
                foreach (var sprint in _completedSprints)
                {
                    velocity += sprint._storyPoint;
                }
                _velocity = (int)(velocity / _completedSprints.Count());
            }

            else
                velocity = 0;

            return velocity;
        }
    }
}

