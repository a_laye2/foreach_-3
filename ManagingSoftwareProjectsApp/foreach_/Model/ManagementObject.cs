﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace foreach_.Model
{
    // this object will contain all of the data for the program: users, stories, sprints, etc.
    public class ManagementObject
    {
        // project items
        public List<TaskItem> taskItems = new List<TaskItem>();
        public List<ProjectItem> projectItems = new List<ProjectItem>();
        // report items
        public List<UserStory> userStories = new List<UserStory>();
        public List<SprintReport> sprintReports = new List<SprintReport>();
        // story items
        public List<Story> genericStories = new List<Story>();

        public string _folderName { get; set; }

        public ManagementObject() { }

        public void SaveProject() { }

    }
}
